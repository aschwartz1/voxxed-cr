VOXXED Microservices 2019 - Paris - 21-22-23 octobre 2019
---

Salut tous ! Je me suis rendu à la conférence [Voxxed Days](https://voxxeddays.com/microservices/) lundi et mardi 21/22 octobre (une 3ème journée est consacrée aux workshops), j'ai pris mes petites notes, dont voici la restitution :

La conférence Voxxed, watisit ? C'est un peu la petite soeur de la conférence Devoxx...

[Devoxx](https://www.devoxx.fr/), watisit ? Une conférence qui parle de toutes les technos récentes et plus, on y voit des REX, pleins de langages, pleins d'outils, plein de gens, c'est pour les devs et les ops, mais aussi les devops, les devsecops, les bizdevsecops, les archis, tous ceux qui s'intéressent à la tech !

Voxxed du coup, c'est donc une autre conférence, un peu plus recentrée sur les technos JVM et Cloud, et les microservices comme l'indique le titre.

La conférence, Voxxed Days, se déroule donc en 3 jours, les 2 premiers sont 2 tracks de conf et le 3ème jour est consacré aux workshops :
 - Quarkus
 - Istio & K8s
 - Spring Security 5
 - Monolith migration

On compte donc 2 tracks, 44 talks, 46 speakers, l'intégralité est filmée et sera disponible sur youtube, je complèterai le document avec les liens quand ils seront disponibles.

 Disclaimer: il y a 20h de prise de notes, j'ai parfois eu le temps d'écouter, parfois de comprendre aussi, au bout de la première journée j'ai tenté de passer des notes au résumé, ce qui a eu pour effet d'améliorer un peu ma prise de note le lendemain matin.  
 Par contre en fin d'après midi la dernière conf et la keynote étaient soit compliquées à comprendre (anglais maché ou complexe), soit à suivre (bon anglais british et super rapide) du coup je n'ai pas réussi à en faire quelquechose de clair à retranscrire ¯\\_(ツ)_/¯  
 Mes commentaires perso ou précisions sont entre \[crochets], j'essaye de ne pas transformer les propos autant que possible. 

# Lundi


## Keynote 9:15-9:45 : Cloud Native: why, oh why?
by Emmanuel Bernard

Keynote d'introduction à la conférence Voxxed Days

<details><summary>CR</summary>
<p>

Il évoque l'évolution du monolith, vers les microservices, et aujourd'hui les fonctions, avec un shift de l'objectif du nombre de req/s vers le nombre de req/s/MB, l'objectif étant de \[mettre un max d'instances sur le cluster\] densifier le cluster de VMs au mieux.

Cette évolution se fait avec un changement dans les schémas de communication entre applications lors du passage au uServices, 
on passe aussi d'un monde scale to 1 à un monde scale to 0, où les coûts deviennent un nouveau levier.

Ces changements sont poussés par la recherche d'une agilité au niveau business (delivrer des features améliorées et nouvelles), et d'une recherche de la réduction du 'Time to deliver' (prévu pour hier, features partielles en A/B test)

Il ouvre ensuite sur un ensemble de challenges à couvrir prochainement :
- Rapidité de développement

_Il relève au passage que les containers scalent, pas les développeurs..._

Pour accélérer les développements, on vise demain à réduire le temps d'intégration d'un nouveau développeur dans l'équipe, comment le rendre opérationnel au plus vite, alors que l'ensemble des technologies changent vite, pour cela on écarte de plus en plus et simplifie les opérations de déploiement, on vise un 'commit to prod', une CI automatisée, self-aware
- Isolation de l'application
Les 12 factors en sont déjà une réponse (configuration externalisée, application stateless, ...)

- Isolation de la donnée
Le passage à du multi cloud engendre des coûts de transferts de données importants, quelles stratégies adopter (copy, data stream, synchro)

Le passage aux microservices modifie les patterns de communications, il faut désormais tenir compte de leur échec. 
- La gestion manuelle de chacun des services devient aussi une tache trop complexe, ainsi que leur sécurisation \[avec plusieurs dizaines services], 
de nouveaux outils continuent d'apparaitre pour faciliter leur mise en oeuvre
- En comparant les technologies dites réactives, les technologies threadées \[historiques] peuvent mal s'adapter à certaines situations, un service indisponible pouvant par exemple épuiser les resources des services qui l'utilisent.
- Alors qu'auparavant on voyait la data au repos (data at rest), on l'imagine désormais en mouvement (data at motion)

Un dernier challenge consiste à densifier au mieux les applications pour en réduire leur coût, java est cher aujourd'hui (en cout mémoire et temps de démarrage), de nouvelles technos viennent remanier le paysage


</p>
</details>



## Keynote 9:45-10:15 : Containerization crossroads: Experiences-Experiments-Expectations
by Ana-Maria Mihalceanu

Ana-Maria s'attache ici aux containers et à l'univers Docker.

<details><summary>CR</summary>
<p>

* Bénéfices de la contenerization  
  * Démarrage instantané
  * Portability  
  * Modularité  
* Principes de design des containers  
  * Single concern  
  * Self containment  
  * Image immutability  

Lors de la mise en place de Docker, elle observe que peu utilisent encore efficacement le système de Layer (en tenant compte de la fréquence des mises à jour de chaque Layer, le risque étant de construire de grosses images de l'ordre du GB)  
Elle conseille pour cela :
* d'utiliser les images officielles
* de ne pas installer les packages dont on n'a pas besoin
* de profiter du cache (en descendant les couches peu mises à jour)
* d'utiliser .dockerignore
* d'employer des build en plusieurs stages pour n'extraire que le nécessaire
* de grouper les lignes de commandes avec && et \\ pour ne créer qu'une seule Layer

Elle évoque enfin Kubernetes et la gestion des resouces, les contraintes rencontrées dans ce domaine :
* limites en nombre de requetes
* affinités et anti-affinités entre nodes et pods
* PodsDisruptionBudget (~réassurance d'un minimum de pods disponibles)
* Horizontal pod scaler VS Vertical pod scaler (incompatibles entre eux)

Elle offre enfin quelques recommandations :
* mettre en place liveness & readyness probes
* renseigner NOTES.txt
* Utiliser Helm lint, Helm test avec les hooks Helm

</p>
</details>

## Conférence 10:15 - 10:45 : Past, Present and Future of Microservices
by Sebastien Stormacq

Points communs et évolutions des microservices, facteurs de réussite

<details><summary>CR</summary>
<p>

Les \[micro-]services ne sont pas une nouveauté, elles ont eu plusieurs versions comme le démontrent les livres sur Corba, les RPCs, les EJBs), on vise à chaque fois des composant réutilisables, aujourd'hui la déclinaison en microservices persiste.  
Ils partagent un 'ubiquitous standard' http + json, non propriétaire, et peu complexe (comparé à Corba par exemple)

* Cost effective runtimes  
  * 20 cts = 1 million d'appels de lambda [le speaker travaille chez AWS]  
  * On met en place bien plus vite docker + lambda aujourd'hui, comparé à l'achat d'un rack par le passé

* les microservices devraient suivre la philosophie unix: do one thing, do it well) et devraient être :  
  * Indépendants  
  * Autonomes  
  * Spécialisés  
  * Single team (agile)  

* Comment gérer la complexité des micro services, du système en entier ?  
  * Le problème n'est pas seulement technique, ils sont d'abord un problème d'organisation  
  * Les systèmes complexes qui fonctionnent sont d'abord (et toujours) issus de systèmes simples.  
  * Ex: Amazon S3, qui passe de 8 à 200 microservices

* Challenges d'aujourd'hui  
  * Observability (Application, Machine, Réseau) => sur les pilliers (Event logs, metrics, tracing)  
  * Discoverability  
  * Communication style  
  * Security (testable, prouvable)  

* Chaos
  * Vérifier comment l'application réagit  
  * Vérifier comment *l'équipe* réagit

* Facteur humain
  * \"You build it, you run it !\"
  * Les petites équipes permettent une meilleure communication et favorisent les pratiques agiles
  * Il faut voir les compétences comme un ensemble d'outils, et préférer la diversités pour un tirer un meilleur bonus global pour l'équipe (~les compétences partagées ne s'additionnent pas)

</p>
</details>

## Conférence 11:15 - 12:00 : Micronaut Deep Dive
by Graeme Rocher

Graeme Rocher vient présenter Micronaut, un framework Java orienté applications cloud et légères

<details><summary>CR</summary>
<p>

* Java & Serverless : Challenges 
  * cold start
  * faible emprunte mémoire

* Limites et problèmes rencontrés en Java
  * API annotation limitée
  * Effacement de type à la compilation
  * Reflexion lente
  * Data cache basé sur la reflection (hibernate)
  * Scan du Classpath
  * Chargement de classes dynamique lent

* Java est rapide !
  * Mais la plupart des outils sont basés sur la reflexion
* Quel est le problème ? => informations obtenues au runtime
  * Android résout déjà ce problème avec la compilation en avance de phase (AOTC ahead of time compilation)
  * L'injection via Dagger ne nécessite pas de réflexion

* Micronaut
C'est un framework pour l'injection, la programmation orientée aspect (AOP), la gestion de la config, les microservices, le serverless, les applications orientées Message (compatible Kafka, RabbitMQ), mais aussi les applications en CLI, les applications Android, tout ce qui a un static main()... sans reflexion
  * Compatible GraalVM
    * disponibilité d'outils pour créer des native-image
    * on obtient 15ms de temps de démarrage, avec une consommation de 14MB

* Démo
  * Ajout de l'annotation @Introspected sur les classes POJOs, peut être détecté automatiquement (par exemple les @Entity)
  * Ajout simple de code pour travailler les classes annotées (quelles que soient les annotations), ce qui génère des classes supplémentaires au lieu de découvrir les annotations au runtime (sans interférer avec le code métier)
On retient que toutes les annotations sont mappées vers du bytecode

* Conclusion
  * Fournit un set de primitives pour développer \[??]
  * Sacrifie le temps de compilation
  * Résoud les problèmes de Spring et JakartaEE (emprunte mémoire + startup time) 
  * Ouvre la porte à GraalVM

</p>
</details>

## Conférence 12:15 - 12:40 : Cloud Run, serverless containers in action
by Guillaume Laforge

https://voxxeddays.com/microservices/2019/08/22/guillaume-laforge-on-cloud-run-serverless-containers-in-action/

Guillaume Laforge nous a présenté Google Cloud Run et nous a présenté les écrans d'administration de ce dernier outil sur l'interface GCP.

<details><summary>CR</summary>
<p>

CloudRun apporte un déploiement en production en quelques secondes, pour n'importe quelle application stateless, n'importe quel langage ou librairie
CloudRun retire le besoin de gestion d'infrastructure, et permet le focus sur l'écriture du code.
CR permet de scale UP rapidement, de scaler à 0, et permet de payer exactement ce que l'on consomme (tranche de 100 ms)
L'outil peut se déployer en standalone sur GCP, sur Anthos, sur Knative
Il est basé sur des technologies open sources

* Container contract
  * temps de requête < 15 min
  * Stateless
  * écoute sur le port $PORT
  * pas de travail en background à la fin de la requête
  * 80 appel concurrents par image -> facturé seulement le temps d'appel, pas en dehors (entre boot et 1er appel) -> meilleure densité, moins de nouvelles instances

* Démo #1
Update de version via build d'une image docker, tag puis push sur un registry (GCR), puis une commande `gcloud beta run deploy ... --platform=managed --allow-unauthenticated --regions=...`. La même opération est réalisable via l'ihm

* Démo #2
Mise à jour du code, ajout d'un controller java recevant des requetes de type POST, et en 2 clics l'application était branchée sur un PUB/SUB et recevait des événements lors de la dépose d'un fichier sur le stockage Google

L'interface permet également d'afficher les logs des instances, de consulter les anciennes révisions (=déploiements)

</p>
</details>

## Conférence 13:00 - 13:15 : 7 Reasons why your microservices should use Event Sourcing & CQRS
by Hugh Mckee

Tout est dans le titre...

<details><summary>CR</summary>
<p>

_0 CQRS kesako ?
* Write : data store rapides
* Read : Optimisé pour les requêtes

_1 Transition douce depuis l'univers DDD et Eventstorming
 1) DDD & Event Storming
 2) Command & Events, Microservices
 3) Event Sourcing & CQRS

_2 Réduction du couplage entre les services
Rend les interactions indirectes, facilite la gestion de la donnée via principalement du 'pulling asynchrone' de données

_3 Sépare le goulot d'étranglement entre lecture et écriture
Les données sont stockées sous forme d'event log (peut aller vite)
Le lecteur avance à son rythme (et gère sa position avec un pull via offset)

_4 Gère correctement certains pics de charge
Historiquement le soucis en base de données était potentiellement dur à prévenir et corriger

_5 Simplfie et renforce la messagerie
En apportant une sémantique du 'delivery' :
* `maybe once` (peut répondre à des besoins business)
* `at least once` (facile à réaliser techniquement)
* `exactly once` (moins facile à obtenir)

_6 Elimine le couplage de service
Evite de faire tomber le système si une brique tombe de manière isolée, les données sont conservées

_7 Eprouvé aujourd'hui dans l'informatique
...

</p>
</details>

## Conférence 13:30 - 14:15 : Quarkus why, how and what - Supersonic.Subatomic.Java.
by Emmanuel Bernard

Emmanuel Bernard nous a expliqué les principes de fonctionnement de Quarkus, nouveau framework Java taillé pour les microservices !

<details><summary>CR</summary>
<p>

* [Quarkus](https://quarkus.io/) est un framework Java 
  * orienté Cloud Native (suit les principes des [12 factors app](https://12factor.net/fr/))
  * destiné à être déployé notamment sur les plateformes d'orchestration (ie. k8s, tout provider cloud)
  * orienté micro-services et haute densité (maximiser l'utilisation d'un container), prévu pour les nouveaux moyens de communications (réactives)
  * compatible serverless

* Démo #1
Emmanuel construit un endpoint avec JAX-RS \[très proche de Spring MVC] [un exemple ici](https://quarkus.io/guides/getting-started-guide). L'application est buildée avec Maven, un mode développeur existe et est activé le temps de la démo, par le biais d'une erreur volontaire, l'outil indique dans les logs de manière efficace l'origine de l'erreur, il corrige la typo, le hot-reload recharge instantanément la page, dans les faits la VM s'est éteinte, le code a été recompilé et la VM redémarrée en une fraction de secondes. 

* Démo #2
Emmanuel étant par ailleurs l'auteur de la librairie Hibernate \[rien que ça!], il présente ensuite son extension pour Quarkus nommée Hibernate with Panache, qui consiste en l'ajout du pattern Active Record sur les entités (au cout d'un héritage de classe pour lesdites entités). Cela permet de définir également les requêtes (HQL) directement au niveau des classes entités. On peut toujours choisir le pattern Repository si l'on préfère.

Il ajoute également un fichier import.sql au projet, détecté et pris en compte automatiquement par le framework et qui a pour effet de peupler l'écran de démo (une todolist)

Il termine la démo avec la compilation `mvn package` qui peut produire une image docker optimisée en tenant compte des Layers

* Why Quarkus ?
Java est un langage destiné au débits élevés (request/s), son adoption dans les microservices est freinée par :
- coût au démarrage (plusieurs secondes pour le chargement d'un grand nombre de classes, le bytecode, initialisation diverses...)
- coût mémoire élevé de chaque instance (réduit fortement le nombre d'instances possibles sur les containers, ou multiplie les coûts)

C'est "un framework conçu pour aider les autres frameworks à réaliser plus de choses lors de la compilation" !

* Bénéfices
  * Configuration unifiée
  * Zéro config, live reload
  * Possibilité de créer des images natives
  * Time to First Response excellent
  * Econome en mémoire (emprunte de 140 Mo sur JVM standard, 130 Mo sur GraalVM, 19 Mo en compilation native)
  * Unifie les paradigmes impératifs et réactifs (les deux sont possibles dans la même application)
  * Best of breed des frameworks et standards Java
Il relève aussi la présence d'une couche d'interprétation pour Spring.

* Déplacement du temps de démarrage à la compilation
Quarkus intervient à la compilation des projets pour déplacer tout le traitement habituellement réalisé dynamiquement au démarrage de l'application vers une compilation statique. (les librairies vont par exemple traduire toutes les annotations dès le build, quarkus va tenter d'initialiser et préparer les connexions externes, seuls les paramètres de configuration restent lues au démarrage)

Les temps de démarrage sont de cette manière réellement réduis à une poignée de millisecondes, l'emprunte mémoire est elle aussi réduite, tout le code nécessaire au traitement dynamique est retiré à la fin du build car il n'est plus utile, de même que le code issu du JDK pour charger les classes devient superflu. On ne retrouve pas (ou très peu) de reflexion et proxy dynamiques. 

* 2 modes de compilation :
- JVM -> Hotspot, graalVM, Open9JDK
- native -> executable autoportant optimisé, coût mémoire optimisés

Quand les utiliser ?

[PHOTO]() // TODO

* Autres points abordés sur la fin
  * Observabilité : le framework contient le nécessaire à l'émission de métriques \[~Spring Actuator]
  * Compatible Kafka, MQTT, Stream (librairies/connecteurs)
  * Maven/Gradle
  * java/kotlin/scala
  * compatible avec Lombok (n'intervient pas au même moment)

* Quelle différence avec Micronauts ?
  * Micronauts embarque et lit le code compilé
  * Quarkus lit uniquement le bytecode lors de l'exécution

</p>
</details>

## Conférence 14:30 - 15:15 : GraalVM and MicroProfile: A Polyglot Microservices Solution
by Roberto Cortez

Roberto Cortez nous a montré les capacités de GraalVM sur son support de différents langages, et comment il est possible d'apporter les fonctionnalités mêmes avancées d'un langage à l'autre

<details><summary>CR</summary>
<p>

_les microservices sont le rêve des architectes, le cauchemard des développeurs_

* GraalVM
  * Machine Virtuelle Universelle
  * Compatible JS, Python, Ruby, R, JVM, LLVM

  // TODO [PHOTO]

* Pourquoi GraalVM ?
  * Apporter une interopérabilité entre les langages
  * Permettre la réutilisation des librairies quel que soit le langage
  * Disposer d'outils partagés
  * Créer des images natives
  * Faire tourner Java plus vite

* Démos
- JS dans Java
- Réutilisation d'une librairie Java et appels de code Java instancié via 2 exemples :
  - NodeJS
  - Ruby 

Une [page web](https://www.graalvm.org/docs/reference-manual/compatibility/) permet de vérifier la compatibilité des librairies node, R et gems Ruby avec GraalVM, les librairies n'étant pas toutes nativement compatibles avec ce dernier.

* Microprofile
\[pour l'anecdote, personne dans l'assemblée n'utilise les microprofiles, personnellement je connais cette distinction en Java, mais ne l'ai jamais vu appliquée où que ce soit, les spécifications étant arrivées largement après la guerre... globalement les gens sont soit restés sur un monolithe, soit utilisent Spring, soit ont changé de langage depuis, en soit elle n'apportent rien de nouveau mais délimitent un cadre de techno standardisées (injection CDI, transactions, metrics) et les annotations nativement présentes en Java. La version micro (par opposition à web et full) est désormais en version 3 depuis juin 2019]
GraalVM s'inscrit dans le cadre définit par Java Microprofile, et permet notamment l'utilisation de toutes ses capacités dans les autres langages (transactions, REST Client, ORM, Config, Fault Tolerance).

Il n'y a pas de surcout au niveau données en mémoire à passer d'un langage à l'autre, étant donné qu'une unique VM prend en charge le code. L'application Java + Runtime pour Node emprunte 71 Mo en mémoire à l'exécution \[léger !]

L'intéret dans la configuration est immédiat car on peut réellement l'unifier et la réutiliser dans l'ensemble des projets, au lieu de prévoir une librairie différente (et des résultats différents) par langage.

* Démo
Réutilisation depuis Python, Ruby, C de retries, fallback et api codée en Java \[=> Microprofile pour tous les langages ?]
* Démo
Exposition d'une fonction R depuis un endopint JAX-RS

* Remarque
  * Compatible Java 8 aujourd'hui, Java 11 prévu pour la fin d'année 
  * GraalVM manque encore de tooling pour le moment
  * Peu de documentation détaillée (car vite obsolète, le projet avance vite)
  * Utilisé par Twitter en production

* Challenge
  * Support de python à compléter
  * Limiter les breaking changes
  * Pret pour Java8 et JS

</p>
</details>

## Tools in action 15:30 - 15:55 : Helm your way with Kubernetes
by Ana-maria Mihalceanu

Informations et démonstration de Helm (v2/v3) sur K8s 

<details><summary>CR</summary>
<p>

* Challenges liés à Kubernetes
  * Gestion et mise à jour de la configuration
  * Déploiement de plusieurs configurations pour fournir une seule application
  * Support d'environnements multiples

* Helm v2
  * Constitué d'un client Helm en ligne de commande
  * Et d'une partie serveur (Tiller) installé sur le cluster K8s

Helm permet le déploiement de configurations rassemblées sour la forme de 'Charts' et gère les mises à jour automatiquement en manipulant le cluster pour qu'il s'adapte à la dernière configuration définie (création de service, scaling de pods, mise en place de routes, images etc...). Helm comprend l'utilisation des namespaces, et permet de définir des charts, ou de réutiliser ceux de la communauté ou des versions partagées par les fournisseurs (base de données, images docker, etc...)

* Helm v3
  * Disparition de Tiller coté serveur au profit de l'api server K8s
  * Reste compatible avec les charts v2 (sauf pour les RBAC)
  * Ajout de la capacité 'uninstall' symétrique à 'install'

* Démo v2
  * Approche 1 chart par service
  * Approche Master chart : 1 chart parent + 1 fichier requirements.yml listant les dépendances
  * (en v3 le fichier requirements.yml disparait au profit d'un bloc dans le chart parent)
  * (Approche hybride : 1 master par service group)

* Attention à :
  * La définition des Job et Template Cron diffère
  * La différence entre v2 et v3 au niveau des RBAC (Rule Based Access Control) peut bloquer un déploiement si on l'oublie

* Take Away
// TODO PHOTO 15h55

</p>
</details>

## Conférence 16:15 - 17:00 : Transactions in your micro-services architecture
by Rudy de busscher

\[Le titre n'étant pas très inspirant au premier abord, je visais initialement l'autre track, mais le speaker n'est jamais arrivé... on a donc manqué le début]

<details><summary>CR</summary>
<p>

Les slides sont disponibles [ici](https://fr.slideshare.net/Payara1/transactions-in-microservices-154925959)

\[Remarque: La présentation étant essentiellement un live coding, la retranscription est ici difficile]


Rudy de busscher nous a montré et expliqué \[trop rapidement] sous la forme d'un endpoint et d'annotations l'utilisation de Java Microprofile LRA (Long Running Action), permettant de jouer des transactions distribuées à travers différents services via un fonctionnement par ack et compensation et l'utilisation d'un coordinateur. Les LRA s'appuient sur la définition des [SAGA](https://microservices.io/patterns/data/saga.html)

* Exemple d'utilisation

\[Je tire un exemple de [cette page](https://medium.com/oracledevs/long-running-actions-for-microprofile-on-helidon-data-integrity-for-microservices-2bd4d14fe955) pour illustrer]

```java
    @Path("/placeOrder")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @LRA(value = LRA.Type.REQUIRES_NEW)
    public Response placeOrder(@HeaderParam(LRA_HTTP_CONTEXT_HEADER) String lraId)  {
        //...
    }

    @Path("/cancelOrder")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Compensate
    public Response cancelOrder(@HeaderParam(LRA_HTTP_CONTEXT_HEADER) String lraId) throws NotFoundException {
        //...
    }

    @PUT
    @Path("/completeOrder")
    @Produces(MediaType.APPLICATION_JSON)
    @Complete
    public Response completeOrder(@HeaderParam(LRA_HTTP_CONTEXT_HEADER) String lraId) throws NotFoundException {
        //...
    }
```

* @LRA indique une méthode participant à une transaction (similaire à @Transactional)
* @Compensate indique la méthode (idempotante) à appeler pour corriger les faits en cas d'échec de la transaction
* @Complete indique la méthode (idempotante) à appeler en cas de succès de la transaction

L'appelant (le coordinateur initie la transaction, orchestre les appels, et signale la fin ou l'échec de la transaction par un système de callback et d'Id unique de transaction pour charger automatiquement le contexte de la requete lors de l'exécution des méthodes

Chaque participant doit gérer son état, qui peut être une base de données (l'application reste stateless)

* @Leave indique qu'une méthode permet de mettre fin à la transaction, l'idée étant d'y placer un chemin possible et normal de la transaction, tel qu'un changement d'avis sur le choix d'un produit (par exemple dans la gestion des stock avec réservation)
* @LRAData permet d'injecter un bean avec les informations (détaillées?) liées à la transaction.

</p>
</details>

## Conférence 17:15 - 18:00 : Event-Driven Microservices, the Sense, the Non-sense and a Way Forward
by Allard Buijze

https://voxxeddays.com/microservices/2019/08/20/allard-buijze-on-event-driven-microservices-the-sense-the-non-sense-and-a-way-forward/

<details><summary>CR</summary>
<p>

\[La présentation démarre sur un ton humoristique, il présente une évolution de l'architecture logicielle, je ne lui donne pas totalement tord. Il met des baffes toutes les minutes sur chaque point qu'il décrit]

* Design Boîte-Boîte-Cylindre
// TODO [Schémas]
  * Architecture Universelle BBC
  * Lorsqu'elle devient trop complexe, on extrait des boîtes
// TODO [Schémas]

* Evolution vers les microservices (de BBC)
  * Application du lift & shift, mêlée à l'apparition d'api isolées, de BBC complexes et isolées en plusieurs boîtes.
 // TODO [Schémas]
  * Multiplication du nombre d'unité de déploiement
  * Noun Service Strategy : on cherche à nommer 
  * Lutte acharnée entre modularité et nombre d'unité de déploiment

Son verdict : _"Want to build microservices ? Learn to build a decent monolith first"_

* A propos des microservices
  * Construire d'abord un monolithe
  * Extraire les briques selon des besoins *métiers*
  * _Location transparency_ : requis, l'application ne doit en aucun cas dépendre de son lieu d'installation
  * Utiliser la communication par événement
    * _Event all the things ? No no no..._ Il est difficile de traduire tout besoin de communication par des événements, les commandes et requêtes trouvent naturellement leur place

* A propos des Messages 
  * Tout message n'est pas Event ! (Tout Event est Message)
  * Il y a les Commands et les Queries

* Event sourcing
  * LA source de vérité, toute la vérité, rien que la vérité
  * On ne stocke plus l'état du système, mais l'ensemble des événements qui conduisent à son état actuel
  * Remarque \[intéressante] : si aucune application ne consomme elle même les événements qu'elle produit, ce n'est pas de l'Event Sourcing ! La présence d'une telle boucle et son utilisation est un cas normal d'utilisation de l'event sourcing, sinon cela correspond à une communication traditionnelle.

* Pourquoi l'Event sourcing
  * Pour répondre à des besoins d'audit, de _compliance_, de transparence
  * Pour résoudre efficacement des questions de data mining, d'analyse de données, obtenir de la valeur de ses données
  * Pour des raisons techniques \[?]
  // TODO [PHOTO 17H41]

* Challenges avec l'Event Sourcing
  * Gérer le stockage (les outils cloud répondent à ce besoin)
  * Difficultés d'implémentation (des outils existent)
  * Penser "événement" (Event Storming) face à l'habitude de penser de manière procédurale

* CQRS
  * _CQRS All the things ?_ 
   // TODO [PHOTO 17H46]

* Communication entre microservices = contrat
  * Les communications (par api REST) reposent de manière bien souvent implicites sur un contrat d'interface
  * Il faut chercher à rendre ces contrat explicites (qui aime être marrié avec un\[e] inconnu\[e] ?)
  * On peut réduire la surface d'exposition des contrat en s'appuyant sur une couche d'anticorruption entre les ..........
   // TODO [PHOTO ]
</p>
</details>

# Mardi

## Conférence 09:00 - 09:45 : Transforming a monolith to an API platform
by Abderrazak Bouadma, Philippe Anes

Retour d'expérience d'une Fintech (Kyriba) (800 personnes dont 200 tech)

<details><summary>CR</summary>
<p>

Point de vue DEV:

Coté business, la demande était de fournir des APIs pour tout besoin, ils ont essayé une approche Domain Driven Design, qu'ils ont renommé Domain Driven Decision. Aidés par Arolla (ils ne disposaient pas de cette compétence en interne), leur premier constat est la difficulté de l'exercice, qui a généré chez eux beaucoup de discussion, cela a été un premier apprentissage pour eux.

Le repository Git avait atteint 500 branches (contenant chacune du métier, pas de branches oubliées)

Leur premier essai a ensuite été un split en différents Repositories via des interfaces Java, pour finalement les ranger dans un ear déployé sur wildfly, de leur propre observation c'est toujours un monolithe.

Leur coup suivant a été OSGi (les microservices n'existaient pas, la promesse était intéressante), résultat ils sont un peu 'seuls' et ont du mal à attirer de nouveaux développeurs. \[l'IDE Eclipse est construit sur une telle plateforme]

_Is a monolith really bad ?_ [PHOTO 9H15]

En vue de refaire leur base de code, quelle étaient les outils pour mettre en place une api REST ?
- Spring
- Rest/Json
- Swagger
Ils utilisaient également Spring dans leut base de code, ce n'était par contre pas suffisant pour partir en production avec seulement ces outils et recherchent une passerelle pour leurs APIs.

Ils optent pour la stack "Spring Cloud Netflix" pour les imiter et rester open source. Lorsque Netflix a abandonné ses outils au profit d'une version purement "Spring", ils ont dû faire de même, cependant cette partie fut simple (pas de breaking change).

Dans les faits, ils sont malgré tout encore sur un monolithe, le nombre de requêtes augmente et faire scaler le monolith n'est pas envisageable.  
Ils ont cherché ensuite à extraire ces APIs du reste contenant le 'métier', ils ne pouvaient par contre pas laisser chaque équipe choisir comment procéder pour rester cohérent avec constellation d'outils pour y parvenir, les difficultés ont été l'absence de configuration centralisée et l'apparition (comme la disparition) fréquente de nouvelles librairies.

La même problématique s'est posée autour de containers docker, différents acteurs existent, ils décident de choisir Kubernetes + Swarm. Le passage a Docker ramène aussi le besoin d'observabilité (logs, métriques, traces)

Ils donnent aussi la possiblité aux équipes évoluer en autonomie, elles ont cependant l'obligation de communiquer et doivent être alignées sur ce qu'elles doivent faire.

Bilan dev : les microservices sont compliqués.

Point de vue OPS:

Situation de départ : plusieurs DC OnPremise + AWS, identités gérées avec LDAP
Pour le build et déploiement, Jenkins + Chef + Rundeck
Leurs instances Jenkins étaient en permanence occupée (24h/24), ce qui engendrait notamment un cout élévé.
Ce coût les a motivé à passer à Amazon EKS ~(+Helm, KubeSpray, OpenPolicyAgency)

L'utilisation de Swarm les a conduit à mettre en place une brique de Discovery faite maison, ce qui les fait sortir des pratiques standards, mais fonctionne pour eux.

Bilan ops : la route des microservices est longue, de nombreux outils sont à gérer, ils sont conscients que l'apprentissage est en cours, et qu'ils ne peuvent pas tout refaire.

Il considèrent qu'il n'y a pas de ballet en argent, et déconseillent de sauter sur les conclusions

\[Navré pour le format et l'enchainement un peu brouillon]
</p>
</details>

## Conférence 10:00 - 10:45 : The Evolution From Monolith to Microservices to Serverless and Beyond
by Hugh Mckee

https://voxxeddays.com/microservices/2019/08/23/hugh-mckee-on-the-evolution-from-monolith-to-microservices-to-serverless-and-beyond/

Hugh parcours l'évolution des style d'écriture des applications, les raisons qui nous ont poussées à effectuer ces changements.

<details><summary>CR</summary>
<p>

Au niveau des unités de déploiement, nous faisons de plus en plus petit, pourquoi ?
-> Améliorer la vitesse de l'innovation technologique.
-> caractéristiques visées : indépendance des déploiements, couplage faible

* Monolith
  * Pour quitter le monolithe, est-on pret et dispose-t-on des outils adéquats ? (est-on capable de un bon monolithe ?)
  * On se heurte à l'époque aux mauvaises pratiques de dev, à un couplage fort du code public

* Microservices
  * On remplace petit à petit le monolithe
  * Courbe d'apprentissage
    * Par l'erreur
    * Evolution graduelle du code
  * Innovation dans un second temps

* Functions (vu comme la décomposition d'un microservice sous forme de fonctions lambda, FaaS)
  * Idéalement, on ne pense plus à surveiller la capacité (les pics de capacités ne posent plus un problème, la caractéristique d'élasticité des VMs n'est plus un point d'attention)
  * Les VMs permettent seulement de compartimenter la capacité
  * Kubernetes vient changer la répartition des applications dans ces VMs, mais l'environnement reste encore contraint en capacité

* Serverless (qu'il renommme *Frictionless*)
  * l'idée est ici de retirer tout ce qui nous distrait autour de la réalisation d'une fonctionalité répondant à un besoin métier
  * La capacité devient uniquement une problèmatique du fournisseur Cloud
  * On peut s'attacher à réduire le coût des fonctions en elles-mêmes (durée, mémoire, processeur)

* Stockage
  * Généralement très difficile à découper lorsque l'on vient d'un monolithe
    * Nombreuses frictions
    * Changements des schémas de données compliqués à gérer
  * Solution : isoler la responsabilité des shémas (inside/outside microservice)
    * Ce qui est interne ne nécessite pas d'explication ou de présentation (facteur d'accélération)

* Pub/Sub
  * Apporte le bus de message entre applications
    * Offre une meilleure résilience au niveau de la donnée
    * Participe au couplage faible et facilite la communication entre les applications

* Functions (nouvelles pratiques), *Flow*
  * Mix synchrone, asychrone, s'appuyant sur les bus de message
  * On observe un cadre d'exécution plus restreint, l'application est composée d'un chainage de fonctions et triggers
  * Apparition des systèmes dirigés par les événements (*Event Driven System*)
  * Le mix des topics et des fonctions apportent une logique de Flux, très naturelle et facile à comprendre

* Beyond ?
  * Les flux tels que définis/utilisés aujourd'hui se composent d'une entrée unique, et d'une sortie unique (en réponse à l'événement)
  * Aller vers un retour global à l'événement qui devient variable, la chaine de fonctions peut à plusieurs endroits donner un retour, s'approchant plus d'un modèle naturel 

  * Stateless functions ? => dans certains scénarios, le principe de récupérer la donnée, la modifier et la sauvegarder peut devenir très couteuse
    * Statefull entities 
    * Statefull serverless
    * L'état est géré dans le cloud, pouvant s'appuyer sur les journaux d'événements

_The biological direction (of design) :_ on se tourne de plus en plus vers une large collections de systèmes de plus en plus petits
 
</p>
</details>

## Conférence 11:15 - 12:00 : Event Sourcing - You are (probably) doing it wrong
by David Schmitz

David présente ici les erreurs les plus courantes rencontrées dans les applications basées sur de l'Event Sourcing, et apporte différents conseils sur ces traitements ainsi que les pièges à éviter.

<details><summary>CR</summary>
<p>

* Bootcamp
  * le monde est dirigé par les événements (Event Driven Business)
  * Chaque événement est un fait, composé de concepts issus du domaine métier
  * Les événements sont stockés relativement au temps
    * Utilisation d'event Store
    * Mise en place de projections pour reconstituer les Aggrégats et offrir un modèle de lecture (par exemple dans une base sql)

* Read models (modèles de lecture)
  * Comment stocker ces modèles, et gérer :
    * Consistence à terme
    * Zero downtime (le rejeu et la reconstruction de la donnée peuvent prendre du temps)
    * Transmissions multiples d'événement et traitement effectivement unique

1 Stream par topic (tout ce qui est relatif à un domaine) ? => complexe à gérer, problème de finesse
* mieux 1 Stream par aggrégat
  * rend les _handlers_ triviaux
  * limite les surcouts opérationnels
  * Quid de la vitesse ? Il indique 100 événements traités en 66ms, la vitesse de traitement n'est pas un problème
  * Si l'on souhaite une requête portant sur "tous les utilisateurs" => il s'agit d'une projection supplémentaire
  * Introduire les modèles de lecture uniquement par le besoin

* Transactions ?
  * Le résultat d'une action dépend de l'ordre des événements
  * L'aggrégat définit les limites de la transaction
  * ⚠️ la validation des données via un modèle de lecture peut conduire à des incohérences
    * Optimistic Concurrency Control
      * On vérifie que l'état initial est toujours vrai, ce qui consiste simplement à vérifier le dernier id d'événement reçu (en lien avec la donnée)

* Gestion des versions (d'événements)
  * Se baser sur du semantic versionning
  * Utiliser la technique de double écriture + 1 handler pour chaque version peut devenir complexe
  * Des librairies (telle qu'Axon) permettent de faciliter ces gestion (lorsque le nombre de version avoisine notamment la centaine)
  * Les personnes ayant la connaissance métier devraient être en mesure de comprendre le contenu d'un événement

* _The lossy event_
  * L'ajout de champs à un type d'événement crée des *trous*
  * Il ne faut pas chercher à les combler (en modifiant le passé), mais plutot produire une nouvelle projection aggrégeant 2 streams

* Archivage
  * Correspond à la production d'un snapshot à l'aide d'un aggrégat
  * ⚠️ ne jamais détruire la source des données (faible coût)

* The _Foo*And*BarEvent
  * Indique 2 données différentes
  * S'en défaire en ajoutant un événement de désactivation, le splitter en 2 streams distincts
  * ⚠️ ne jamais modifier un événement, préférer l'ajout d'événements de compensation (cancellation event)

* Conformité GDPR
  * Comment garantir l'anonymat et interdire l'accès à la donnée ?
    * Chiffrer les données, et jeter la clé pour empecher leur traduction, mais la gestion des clés peut être compliquée
    * Autre idée : Gérer 2 Streams pour les données utilisateur (séparer les données publiques des données privées), et ne détruire que les données privées

* Take away 
[PHOTO 11h58]

Il invite à la fin de son talk à consulter [cette page](https://eventmodeling.org/posts/what-is-event-modeling/) autour de l'Event Modeling, qui détaille une manière de travailler avec les événements

</p>
</details>

## Tools in action 12:15 - 12:40 : Pact tests: how we split up the monolithic deploy
by Phil Hardwick

Phil présente la librairie Pact, permettant le Contract Driven Development (Consumer Driven Contracts), ou comment travailler autour de nos APIs avec confiance lorsque l'on souhaite sereinement déployer des composants de manière indépendante. 

<details><summary>CR</summary>
<p>

Situation : lorsque l'on souhaite mettre à jour un composant, et que l'on entend la question "Quelqu'un a-t-il cassé quelquechose ?
-> Ne pousse pas ce truc, cet autre truc n'est pas pret, en cas d'erreur le rollback (rebuild) peut être long

Comment assurer le bon matching de l'ensemble des APIs lorsqu'on embarque plusieurs évolutions ?
-> On se retrouve dans la situation où l'on reteste des choses qui n'ont pas changé, alors que l'on souhaite surtout vérifier les nouvelles fonctionnalités

Remarque : ce type de vérification n'a pas lieu dans un monolithe

Sans tests de contrats, la solution actuelle est de démarrer le projet dans un environnement dédié, ce qui est lourd et peut être très compliqué à mettre en place (notamment suite aux dépendances externes)

* Contracts
Les contrats d'interface \[pour les API web, et les données] ont toujours été présents de manière implicite, on cherche ici à les rendre à nouveau explicites et les valider de manière plus simple

Le concept derrière la librairie Pact fonctionne entre 2 entités (Consumer & Provider) s'accordant autour d'un contrat.
Les entités étant versionnées (de même que le contrat), les évolutions et la validation de ces contrats se visualise sous la forme d'une matrice et indique clairement si les versions des différentes entités sont compatibles entre elles.

[TODO PHOTO 12h27]

Utiliser un tel outil demande un effort d'organisation, il est nécessaire de s'accorder en premier sur le contrat d'API
  * on garanti techniquement que consommateur et provider se comprennent lors du run
  * Le provider peut être son propre consommateur (avro + schema registry)
  * On teste le format des données uniquement (pas le bon fonctionnement de l'authentification par exemple dans le cas d'une api web)
  * Contract First, drivé par le besoin
  * Utilisation intensive des outils d'intégration continue
    * Les validations sont effectuées entre le consommateur et l'ensemble des versions d'API afin de constituer la matrice de compatibilité
  * Prévoir des tests parfois longs
  * La mise en place de Pact demande de la vigilance afin de ne pas gêner les développement et produire de "bons tests"

La mise en place peut être compliquée, on en tire cependant une assurance sur le déploiement en toute indépendance de briques logicielles (il n'est pas forcément utile de relivrer le producteur de la donnée lorsque le consommateur évolue)
⚠️ On teste le format des requêtes et réponses, le type des champs, mais pas leur valeur (relatif au métier, couvert par les tests unitaires)

</p>
</details>

## Byte Size 13:00 - 13:15 : An introduction to Apache Kafka - this is Event Streaming, not just Messaging
by Katherine Stanley

Quicky autour de Apache Kafka

<details><summary>CR</summary>
<p>

Apache Kafka est un outil Open Source de streaming distribué disposant d'une communauté grandissante

* Message queuing vs Event Stream (Caractéristiques)
  * Message queue
    * Persistence d'un état
    * Les messages sont consommés puis détruits
    * La délivrabilité des messages est garantie
    * Communication Point à Point
  * Event Stream
    * Constitue un historique de la donnée
    * Consommation multiple (scalable) et durable
    * Hautement disponible
    * Abonnement à un topic (Pub/Sub)

* Kafka dispose d'un client Java
  * KafkaConsumer<K,V>
  * KafkaProducer<K,V>

* Mise en place d'un producteur de la donnée
  1) Définir un topic dédié
  2) Décider si une clé de partitionnement est nécessaire (pas utile dans les cas simples)
  3) Choisir un niveau d'ACK (fire-forget, wait 1 broker, wait all)
  4) Décider d'autoriser les rejeu de la donnée dans le flux, impliquant la présence de duplicat dans les données ou non

* Mise en place du consommateur
  1) Choisir le topic à suivre
  2) Choisir où démarrer dans ce topic (tout lire, ou à partir d'un offset)
  3) Choisir comment persister l'offset
  4) Choisir un 'consumer group' (la clé de partitionnement intervient ici pour consommer une partition en particulier)

⚠️ L'ordre est garanti par partition.

* Réplicas
  * Les copies sont transparentes, basées sur un système de Leader et Followers, avec réélection si perte du leader
  * ⚠️ Risque si l'on attend un ACK de la part de tous les serveurs

Il existe actuellement divers outils pour se plugger sur des systèmes Kafka externes (Kafka Connect)
 
</p>
</details>

## Conférence 13:30 - 14:15 : Monitoring in the Microservices world - Kubernetes Vs. Serverless based applications
by Erez Berkner (Lumigo)

Quelles sont les nouveaux besoins et techniques de monitoring mises en oeuvre dans les applications Kubernetes et plus récemment serverless ?

\[Remarque: la société Lumigo développe un produit de monitoring pour les applications serverless, le talk s'accompagne de la mise en application de leur outils, permettant une visualisation plutôt efficace dans ces environnements]

<details><summary>CR</summary>
<p>

* Pourquoi monitorer ?
  * Savoir lorsque quelquechose se passe mal
  * Les erreurs ont un impact business, les utilisateurs n'hésitent pas à changer de site
  * comprendre les impact business
  * Trouver la cause initiale *rapidement* 

* Alertes
  * Quelles alertes ? (celles qui ont un impact sur le business)
  * Métriques spécifiques selon le besoin métier
  * Comment les maintenir ?

* Sévérité
  * Quels sont les services [critiques] impactés par la panne ?
  * Quel est le chemin du problème dans le SI ?

TODO [PHOTO 13h44]

Les outils de logging et monitoring actuels permettent de zooer sur une panne spécifique, d'en extraire les données.
Les outils de tracing distribués permettent par dessus cette base de comprendre le cheminement de l'execution qui conduit à cette erreur.

* Kubernetes (the 5 layers)
Le suivi de cette plateforme s'effectue sur 5 niveaux :
  * Logs applicatifs
  * Readyness/Availability
  * Santé du pod
  * Santé du cluster (capacité, CPU/mem globale)
  * Infrastructure (Hardware -> signes vitaux)
Ces données sont relativement standardisées et faciles à suivre  
Elles sont également intégrés à des outils plus avancés (ex: Istio Control Plane)

* Outils de logs
[TODO PHOTO]
La volumétrie des logs rend impossible leur consultation manuelle

* Serverless (impacts)
  * Tous les services utiles sont désormais disponibles sur le cloud
  * Services de logs managés
  * Paradigme de coût (le design drivé par le coût devient possible)

* Serverless (challenges)
  * Identifier et résoudre les problèmes dans un environnement distribué
  * Comprendre les coûts
  * Visivilité asynchrone des problèmes : observer un point unique n'est pas une bonne solution

* Quelles sont les options 
  * Cloudwatch et produits similaires 
    * Disponibilité out-of-the-box de ces produits
    * Prend du temps pour son exploitation (on perd le gain en temps apporté par la solution cloud)
  * Solutions customs
    * Génération de correlationID
    * Transmission du correlationID auprès de tous les services pour l'inclure dans tous les logs
    * Identification plus rapide avec des outils plus avancés de tracing (Zipkin+Jaeger, OpenTracing, OpenCensus)
      * \+ Solutions non spécifiques à un Provider Cloud
      * \+ Supporté par les différents providers
      * \- Impact dans le code
      * \- Comment intégrer ces outils dans les services managés ?
  * Solutions Saas
  \[Mise en avant de Lumigo]
    * Permet un drill down sur une stack trace virtuelle
    * Présente sur un schéma l'image complète de l'appel au travers des différents services, l'ensemble des briques traversées
    * Lumigo CLI: série d'outils de tracing pour AWS

* Comparaison du logging Container/Serverless

|                           | Container                      | Serverless                                      |
|---------------------------|--------------------------------|-------------------------------------------------|
| Tracing distribué         | Agent                          | Outils modernes (basés sur des APIs ou du code) |
| Calcul du coût            | Par ressource                  | Par requête                                     |
| Que suit-on avec les logs | Hardware, OS, k8s, application | application uniquement                          |
| Responsabilité du suivi   | vous                           | vous                                            |

</p>
</details>

## Conférence 14:30 - 15:15 : Looking back on migrating 30 microservices to a monorepository
by Darya Talanina

Retour d'expérience sur une migration de 30 repositories Git vers un monorepository, elle revient sur les difficultés présentes avant cette migration, et présente la démarche suivie.

\[Remarque: ce talk était un peu étrange, il ne présente rien de nouveau, on part d'une situation legacy assez mauvaise, on arrive au final à un mono repository, mais le talk présente simplement comment la migration Git s'est opérée en conservant son historique. On constate que différents choix techniques non mesurés à l'époque ont conduit à cette situation intenable pour la quinzaine de devs en place. Ce talk aurait été utile il y a 10 ans, on observe simplement une entreprise en difficultés techniques qui persiste dans le monolithe et se satisfait de solutions maisons pour réaliser leur intégration continue, dont la mise à l'échelle reste peu envisageable car certains points ne sont pas résolus]

## Tools in action 15:30 - 15:55 : riffing on Kubernetes
by Florent Biville

https://voxxeddays.com/microservices/2019/08/29/florent-biville-on-riffing-on-kubernetes/

Florent présente ici Riff, librairie/outil permettant \[bientôt] de déployer applications et fonctions serverless sur Kubernetes et Knative, à distance et en local.

<details><summary>CR</summary>
<p>

⚠️ Mise en garde : l'outil présenté n'est pas encore Production Ready

Riff est un outil permettant de créer des Function (FaaS) directement sur un cluster Kubernetes.
Sous le capot, l'outil se sert de [buildpacks.io](https://buildpacks.io/), qui s'occuppe d'ajouter les différentes couches permettant de passer du code à sa version déployée.

* Démo
  * Détection du langage utilisé (Java, Js, bash, ...)
  * Création d'une image docker
  * Analyse de l'image avec l'outil en CLI *dive*, qui permet de visualiser les couches de l'image Docker, on observe que le code applicatif est bien sur la dernière couche (et respecte les bonnes pratiques docker)

* Runtime
  * Riff Core
    * Génère le 'service' et 'déploiement' kubernetes
  * KNative
    * K8s, batteries incluses :)
    * Scaling up et scaling down to Zero (requiert du code applicatif avec un temps de démarrage court pour gérer correctement la charge, au risque de démarrer plus d'instances que nécessaire)
    * Crée une configuration Knative et les routes nécessaire (sauf si on souhaite apporter sa propre configuration)
  * Riff Streaming (nouveauté, non stable, non releasé)
    * [TODO PHOTO 15h45]
    * Permet de réaliser et déployer facilement des flux de données, composés de 'source', 'sink' et 'processor'
    * Supporté en Java et JS par le biais de librairies réactives

* Riff
  * Permet de déployer facilement
    * Des fonctions
    * Des applications (Spring Boot ou autre)
    * Des containers
  * *On peut ainsi déployer facilement une application serverless en local (Minikube)*

Twitter: @projectriff  
(slack.)projectriff.io

</p>
</details>

## Conférence 16:15 - 17:00 : Complex event flows in distributed systems
by Bernd Rücker

https://voxxeddays.com/microservices/2019/09/19/bernd-rucker-on-complex-event-flows-in-distributed-systems/

Bernd présente plusieurs idées visant à maitriser la complexité des flux d'événements dans les systèmes distribués, en partant de solutions naïves à des solutions plus solides (machine à état, BPMN)

<details><summary>CR</summary>
<p>

Il prend pour exemple un achat sur un site de vente, en 3 étapes : pay item, fetch item & ship item, on parle alors de 4 bounded contexts (3 + checkout)
L'exercice est de fournir un bouton qui clignote si le produit est livrable sous 24h

Il discute ensuite des différentes solutions pour y parvenir :
* Implémentation naïve en requete/réponse
  * Impose un couplage temporel du système
  * Entraine une forte charge
* Basée sur les événements
  * le service de checkout écoute les événements 'produit'
  * une autre version s'appuie sur un service de notification distinct qui filtre et pousse les événements au service

* Peer-to-peer event chain
La dernière implémentation aboutie à un tel fonctionnement, le problème qui apparait est qu'on n'est plus en mesure de comprendre le business localement (on perd de vue le flux, à terme le contrôle sur les différents flux)
  * 'Pinball machine architecture' -> détecté avec la question 'Que vient-il de se passer avec ce service?'
  * La solution existe déjà avec le tracing distribué (qui peut être trop fin dans les détails)
  * Autre solution : les outils de Data Mining (mais ces derniers ne sont pas faits/compatibles avec les microservices)

* Il est par ailleurs aussi très difficile de changer l'ordre des événements dans une telle chaîne (si l'on veut faire comme Amazon et passer l'envoi avant le payement)
  * Il n'y a pas de conducteur central
  * Le système doit se voir comme une chorégraphie d'événements, chacun est autonome (mais dans la pratique c'est compliqué, on est souvent à côté de la cible)
  * Où placer le couplage entre les services ?
    * Il suggère d'utiliser les commandes en plus des événements pour y parvenir (aide à éviter les chaines d'événements)
    * L'idée est d'approcher un systême type ESB (smart endpoints, dumb pipes)
    * Le risque est de mettre en place un GOD-service + un ensemble de services CRUD

* Long Running Process
  * Comment éviter la gestion d'état ?
    * Avec des moteurs de workflow (sans lien avec le low code qu'il nomme 'death by property panel'), inconvénients: ils sont complexes et souvent non open source
      * AWS Step Functions
      * Apache AirFlow
      * Activity
      * Zeebee (Camunda)
      * jBPM (BPMN)

* State machine
  * On peut persister l'état pour faciliter les traitements

* Systèmes distribués
  * Stateful retries (la requête comme la réponse peuvent échouer dans un système synchrone, peut-on vraiment tout rejouer sans erreur ?)
  * Il déconseille fortement l'utilisation de transactions dans un système distribué, envisager les compensation (issues des Saga)

Un tel systême (moteur de workflow) est utilisé et éprouvé chez Zalando \[où il a travaillé]

</p>
</details>

## Closing Keynote 17:15 - 18:00 : Reacting to the future of application architecture
by Grace Jansen

https://voxxeddays.com/microservices/2019/08/28/grace-jansen-on-reacting-to-the-future-of-application-architecture/

Grace effectue un rapprochement entre les applications réactives, et les abeilles et leur manière de fonctionner.

# Remarques sur l'événement

La conférence Voxxed, dans sa déclinaison française a eu lieu pour la première fois l'an dernier, il est encore d'une taille modeste mais concentre un ensemble de sujets intéressants autour du monde Java et du Cloud. Une poignée de sponsors étaient présents, je n'ai malheureusement pas remporté la Nintendo Switch mise en jeu par l'un d'eux.

Par rapport au prix du ticket, on aurait pu s'attendre à mieux côté restauration (les portions étaient relativement maigres), les apéritifs ont vite disparu ou se limitaient à diverses madeleine (notez que je n'ai rien contre les madeleines, c'est juste que la prise de notes bin ça creuse).